import java.io.*;
import java.util.*;

/**
 * В классе описана логика решения тестового задания №2.
 *
 * Логика работы :
 * 1) Читает текстовый файл, в котором есть значения от 0 до 20 в произвольном порядке, разделенные через запятую без пробелов.
 * 2) Фильтрует значения по возрастанию или убыванию.
 * 3) Выводит результат в консоль.
 */
public class TaskSecond {

    public static void main(String args[]) {
        String filePatch = "numbers.txt";

        sortAscendingOrDecreasingOrder(filePatch, true);

        sortAscendingOrDecreasingOrder(filePatch, false);
    }

    /**
     * Выводит в консоль отфильтрованные значения цифр по возрастанию или убыванию, содержащиеся в указанном файле.
     * @param filePatch   - путь к файлу.
     * @param isAscending - true: нужно отсортировать по возрастанию; false: нужно отсортировать по убыванию.
     */
    private static void sortAscendingOrDecreasingOrder(String filePatch, boolean isAscending) {
        String textFile           = readFile(filePatch);
        List<Integer> listNumbers = textToArrayInt(textFile);

        StringBuilder result = new StringBuilder();
        if (isAscending) {
            Collections.sort(listNumbers);
        } else {
            Collections.sort(listNumbers, Collections.reverseOrder());
        }

        for (Integer number : listNumbers) {
            result.append(String.format("%d,", number));
        }
        System.out.println(result.toString().substring(0, result.toString().length() - 1));
    }

    /**
     * Преобразует строку состоящую их цифр и разделенных запятыми в список цифр.
     * @param text  - строка, в которой содержатся цифры через запятую.
     * @return      - список цифр.
     */
    private static List<Integer> textToArrayInt(String text) {
        String[] numbers = text.split(",");
        List<Integer> integerList = new ArrayList<>();

        for (String number : numbers) {
            integerList.add(Integer.parseInt(number));
        }

        return integerList;
    }

    /**
     * Возвращает весь текст, содержащийся в файле.
     * @param filePatch - путь к файлу.
     * @return          - текст находящийся в файле.
     */
    private static String readFile(String filePatch) {
        StringBuilder resultText = new StringBuilder();

        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(filePatch)))){

            String line;
            while ((line = reader.readLine()) != null) {
                resultText.append(line);
            }
        } catch (IOException e) {
            System.out.print("Ошибка чтения файла.");
        }

        return resultText.toString();
    }
}
