import java.math.BigInteger;

/**
 * В классе описана логика решения тестового задания №3.
 *
 * Логика работы :
 * 1) Считает факториал от 20 и выводит результат в консоль.
 */
public class TaskThird {

    public static void main(String args[]) {
        System.out.println(factorial(20));
    }

    /**
     * Возвращает факториал числа n.
     * @param n - число у которого нужно посчитать факториал.
     * @return  - значение факториала.
     */
    private static BigInteger factorial(int n) {
        if (n < 0) {
            throw new AssertionError("Факториала от целых отрицательных чисел не существует.");
        }

        BigInteger bigInteger = BigInteger.ONE;
        for (int i = 1; i <= n; ++i) {
            bigInteger = bigInteger.multiply(BigInteger.valueOf(i));
        }

        return bigInteger;
    }
}
